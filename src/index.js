import style from './ParaViewDownload.mcss';
import rootTemplate from './template.hbs';
import contentTemplate from './download.hbs';
import versionsTemplate from './versionSelector.hbs';
import icons from '../icons';

// ----------------------------------------------------------------------------
const USE_LISTING_FILE = true; // False for Apache dynamic listing
// ----------------------------------------------------------------------------

const VERSION_DROP_DOWN = '.version-selector';
const WEB_CONTENT = '.download-content';
const WEB_FILTERS = `.${style.filterBy}`;
const WEB_LINKS = `.${style.entry}`;
const WEB_DESCRIPTION = '.js-description';
const WEB_SECTION = '.js-section';
const WEB_VERSION_SELECTOR = `select.${style.versions}`;

const IS_PARAVIEW_ORG = location.host === 'www.paraview.org';
const ROOT_CONTAINER = IS_PARAVIEW_ORG
  ? document.querySelector('.entry-content')
  : document.querySelector('body');

// ----------------------------------------------------------------------------

const NO_FILTER = 'Always visible';
const FILE_DATA = {};
const VERSION_TO_NUMBER = {};
const SORTED_VERSIONS = [];

let CURRENT_VERSION = null;

const URL_PARAMS = new URLSearchParams(window.location.search);

// ----------------------------------------------------------------------------
// Network - listing parsing
// ----------------------------------------------------------------------------

function fileSizeToString(nbBytes) {
  let reducedSize = nbBytes;
  const units = ['', 'k', 'M', 'G'];
  const unitStep = 1000; // Instead of 1024
  while (reducedSize > unitStep) {
    reducedSize /= unitStep;
    units.shift();
  }
  return `${reducedSize.toFixed(1)}${units[0]}`;
}

function parseListing(content, parseFunctions) {
  const fileListing = [];

  content.split('\n').filter(parseFunctions.isFileOrDirectory).forEach(line => {
    const name = parseFunctions.getListingName(line);
    const file = parseFunctions.isFile(line);
    const date = parseFunctions.getDate(line);
    const size = parseFunctions.getSize(line);
    const version = parseFunctions.getVersion(line);
    const entry = { name, file, date, size };
    if (version) {
      entry.version = version;
    }
    if (name === 'Parent Directory') {
      return;
    }
    fileListing.push(entry);
  });

  return fileListing;
}

// ----------------------------------------------------------------------------
// File - listing parsing
// $ find . -type f -ls | awk '{print $11,$8,$9,$10,$7}' | grep ./v[0-9] > listing.txt
// ----------------------------------------------------------------------------

const TEXT_FIND = {
  isFileOrDirectory(line) {
    const fileTokens = line.split(' ')[0].split('/');
    return fileTokens.length === 3 && ['RCs', '*RC-*', 'RC'].indexOf(fileTokens[2]) === -1;
  },
  isDirectory(line) {
    return false;
  },
  isFile(line) {
    return !!TEXT_FIND.getVersion(line);
  },
  getListingName(line) {
    return line.split(' ')[0].split('/').slice(-1)[0];
  },
  getDate(line) {
    const tokens = line.split(' ');
    tokens.shift();
    tokens.pop();
    return tokens.join(' ');
  },
  getSize(line) {
    return fileSizeToString(Number(line.split(' ').slice(-1)[0]));
  },
  getVersion(line) {
    const version = line.split(' ')[0].split('/')[1];
    return version && version[0] === 'v' ? version : null;
  },
  extractVersions(txtContent) {
    const versions = { nightly: true };
    txtContent.split('\n').map(TEXT_FIND.getVersion).filter(name => !!name).forEach(name => {
      versions[name] = true;
    });
    return Object.keys(versions);
  },
};

// ----------------------------------------------------------------------------
// Apache - listing parsing
// $ find . -ls -type f | awk '{print $11,$8,$9,$10,$7}' > ~/fileListing.txt
// ----------------------------------------------------------------------------

const HTML_APACHE = {
  isFileOrDirectory(htmlLine) {
    return htmlLine.indexOf('<tr><td') === 0;
  },
  isDirectory(htmlLine) {
    return htmlLine.indexOf('alt="[DIR]"') !== -1;
  },
  isFile(htmlLine) {
    return !(htmlLine.indexOf('alt="[DIR]"') !== -1);
  },
  getListingName(htmlLine) {
    return htmlLine.split('</a>')[0].split('>').slice(-1)[0];
  },
  getDate(htmlLine) {
    return htmlLine.split('</td')[2].split('>').slice(-1)[0];
  },
  getSize(htmlLine) {
    return htmlLine.split('</td')[3].split('>').slice(-1)[0];
  },
  getVersion(htmlLine) {
    return null;
  },
  extractVersions(htmlContent) {
    return parseListing(htmlContent, HTML_APACHE)
      .filter(e => !e.file)
      .map(e => e.name)
      .filter(name => name[0] === 'v' || name === 'nightly/')
      .map(name => name.slice(0, -1));
  },
};

// ----------------------------------------------------------------------------
// UI handling methods
// ----------------------------------------------------------------------------

function updateURLParams(obj) {
  Object.keys(obj).forEach(name => {
    URL_PARAMS.set(name, obj[name]);
  });
  return URL_PARAMS.toString();
}

function openPage() {
  const url = new URL(window.location);
  url.search = URL_PARAMS.toString();
  window.open(url.toString(), '_blank');
}
window.openPage = openPage;

function tagsFiltering(tagsStr, filterTags) {
  if (!filterTags) {
    return true;
  }
  const srcTokens = tagsStr.split(',');
  const filterTokens = filterTags.split(',');
  let count = 0;
  for (let i = 0; i < filterTokens.length; i++) {
    const filter = filterTokens[i];
    if (filter[0] === '!') {
      if (srcTokens.indexOf(filter.substring(1)) === -1) {
        count++;
      }
    } else {
      if (srcTokens.indexOf(filter) !== -1) {
        count++;
      }
    }
  }
  return count === filterTokens.length;
}

function updateLinkVisibility(selectedOS, categories) {
  const SHOW = 'block';
  const HIDE = 'none';
  const otherButtons = ROOT_CONTAINER.querySelectorAll(WEB_FILTERS);
  for (let i = 0; i < otherButtons.length; i++) {
    otherButtons[i].classList.remove(style.active);
    if (otherButtons[i].dataset.filter === selectedOS) {
      otherButtons[i].classList.add(style.active);
    }
  }

  const entries = ROOT_CONTAINER.querySelectorAll(WEB_LINKS);
  for (let i = 0; i < entries.length; i++) {
    entries[i].style.display = tagsFiltering(entries[i].dataset.tags, categories) &&
      (entries[i].dataset.os === selectedOS || entries[i].dataset.os === NO_FILTER)
      ? SHOW
      : HIDE;
  }

  // handle section visibility
  const sections = ROOT_CONTAINER.querySelectorAll(WEB_SECTION);
  for (let i = 0; i < sections.length; i++) {
    const osList = sections[i].dataset.os.split(',').filter(v => !!v);
    // Always show the ParaView section otherwise everything could be hidden if
    // nothing is available for your OS and at that point you can not pick another OS
    if (osList.length && sections[i].dataset.category !== 'ParaView') {
      const visible = osList.indexOf(selectedOS) !== -1;
      sections[i].style.display = visible ? SHOW : HIDE;
    }
  }

  // handle description visibility
  const descriptions = ROOT_CONTAINER.querySelectorAll(WEB_DESCRIPTION);
  for (let i = 0; i < descriptions.length; i++) {
    const osList = descriptions[i].dataset.os.split(',').filter(v => !!v);
    if (osList.length) {
      const visible = osList.indexOf(selectedOS) !== -1;
      descriptions[i].style.display = visible ? SHOW : HIDE;
    }
  }
}

// ----------------------------------------------------------------------------

const TAGS_STYLE = {
  LATEST: {
    color: 'green',
  },
  RC: {
    color: 'orange',
  },
  SELECTION: {
    border: 'solid 1px #ccc',
    background: '#eee',
    margin: '2px',
  },
};

function applyTagsStyles() {
  const entries = ROOT_CONTAINER.querySelectorAll(WEB_LINKS);

  for (let i = 0; i < entries.length; i++) {
    const tags = entries[i].dataset.tags.split(',');
    tags.filter(tag => TAGS_STYLE[tag]).forEach(tag => {
      Object.keys(TAGS_STYLE[tag]).forEach(key => {
        entries[i].style[key] = TAGS_STYLE[tag][key];
      });
    });
  }
}

// ----------------------------------------------------------------------------

function filterBy(event) {
  const filter = event.target.dataset.filter;
  updateURLParams({ filter });
  updateLinkVisibility(filter);
}

// ----------------------------------------------------------------------------
// Generic helpers
// ----------------------------------------------------------------------------

function getCurrentOS() {
  if (navigator.appVersion.indexOf('Win') !== -1) {
    return 'Windows';
  } else if (navigator.appVersion.indexOf('Mac') !== -1) {
    return 'macOS Intel';
  } else if (
    navigator.appVersion.indexOf('X11') !== -1 ||
    navigator.appVersion.indexOf('Linux') !== -1
  ) {
    return 'Linux';
  }
  return null;
}

let CURRENT_OS = getCurrentOS();

// Refine OS if mac for arch selection
if (CURRENT_OS === 'macOS Intel') {
  try {
    navigator.userAgentData.getHighEntropyValues(['architecture']).then(v => {
      if (v && v.architecture === 'arm') {
        CURRENT_OS = 'macOS Silicon';
      }
    });
  } catch(e) {
    // Firefox or Safari
    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
      // can be wrong since silicon only exist after macOS 10.15
      //if (!navigator.userAgent.match(/OS X 10_([789]|1[01234])/)) {
      //  CURRENT_OS = 'macOS Silicon';
      //}
    } else {
      // firefox
      const ctx = document.createElement("canvas").getContext("webgl");
      const debug = ctx.getExtension('WEBGL_debug_renderer_info');
      const graphic = debug && ctx.getParameter(debug.UNMASKED_RENDERER_WEBGL) || "";
      if (graphic.match(/Apple/) && !graphic.match(/Apple GPU/)) {
        CURRENT_OS = 'macOS Silicon';
      }
    }
  }
}

// ----------------------------------------------------------------------------

function fetchText(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.onreadystatechange = e => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200 || xhr.status === 0) {
          resolve(xhr.responseText);
        } else {
          reject(xhr, e);
        }
      }
    };

    // Make request
    xhr.open('GET', url, true);
    xhr.responseType = 'text';
    xhr.send();
  });
}

// ----------------------------------------------------------------------------

function strcmp(a, b) {
  return a === b ? 0 : a > b ? 1 : -1;
}

// ----------------------------------------------------------------------------

function entrySort(a, b) {
  let diff = strcmp(a.os, b.os);
  if (diff === 0) {
    diff = -strcmp(a.date, b.date);
  }
  if (diff === 0) {
    diff = -strcmp(a.name, b.name);
  }
  return diff;
}

// ----------------------------------------------------------------------------

function getVersionNumber(label) {
  if (label === 'nightly') {
    return 99999999;
  }
  const versionArray = label.substring(1).split('.').map(Number);
  while (versionArray.length < 3) {
    versionArray.push(0);
  }
  const number = versionArray.map((v, i) => v * Math.pow(10, 6 - 2 * i)).reduce((a, b) => a + b, 0);
  return number;
}

// ----------------------------------------------------------------------------

const FILTER_BY_ORDER = [
  'Sources',
  'Windows',
  'Linux',
  'macOS Silicon',
  'macOS Intel',
  'Irix',
  'Solaris',
  'HPUX',
];
const NO_OS = FILTER_BY_ORDER[0];

function filterBySort(a, b) {
  return FILTER_BY_ORDER.indexOf(a) - FILTER_BY_ORDER.indexOf(b);
}

// ----------------------------------------------------------------------------
// Tags
// ----------------------------------------------------------------------------

const RC = [/RC[0-9]+/, /5.13.0-[0-9]+-/];
const PARAVIEW = [/paraview-/i];
const DOCUMENTATION = [/api/i, /doc/i, /tutorial/i, /guide/i, /\.pdf$/i, /\.rtf$/i];
const OFFSCREEN = [/OSMesa/i, /EGL/i];
const MPI = [/mpi/i];
const CATALYST = [/catalyst/i];
const DATA = [/data/i];
const PLUGIN = [/plugin/i];
const LATEST = [/latest/i];
const VELOVIEW = [/veloview/i];

const TAGS = [
  { label: 'RC', patterns: RC },
  { label: 'PARAVIEW', patterns: PARAVIEW },
  { label: 'DOC', patterns: DOCUMENTATION },
  { label: 'OFFSCREEN', patterns: OFFSCREEN },
  { label: 'MPI', patterns: MPI },
  { label: 'CATALYST', patterns: CATALYST },
  { label: 'DATA', patterns: DATA },
  { label: 'PLUGIN', patterns: PLUGIN },
  { label: 'LATEST', patterns: LATEST },
  { label: 'VELOVIEW', patterns: VELOVIEW },
  { label: 'SELECTION', patterns: [new RegExp(URL_PARAMS.get('selection'))] },
];

// ----------------------------------------------------------------------------
// OS Tags
// ----------------------------------------------------------------------------

const OS_WINDOWS = [/win/i, /\.exe$/i];
const OS_MAC = [/(tiger|universal|osx|darwin)(?!.*arm)/i];
const OS_MAC_ARM = [/(osx|darwin).*(arm)/i];
const OS_LINUX = [/linux/i, /ubuntu/i];
const OS_IRIX = [/irix/i];
const OS_SOLARIS = [/sparc/i, /sunos/i];
const OS_HPUX = [/hpux/i];

const OS = [
  { label: 'macOS Silicon', patterns: OS_MAC_ARM },
  { label: 'macOS Intel', patterns: OS_MAC }, // Need to happen after arm
  { label: 'Windows', patterns: OS_WINDOWS }, // Need to happen after darWIN
  { label: 'Linux', patterns: OS_LINUX },
  { label: 'Irix', patterns: OS_IRIX },
  { label: 'Solaris', patterns: OS_SOLARIS },
  { label: 'HPUX', patterns: OS_HPUX },
];

// ----------------------------------------------------------------------------
// ARCH Tags
// ----------------------------------------------------------------------------

const ARCH_X86 = [/x86/i];
const ARCH_ARM = [/arm/i];

const ARCH = [{ label: 'Arm', patterns: ARCH_ARM }, { label: 'X86', patterns: ARCH_X86 }];

// ----------------------------------------------------------------------------
// Category handling
// ----------------------------------------------------------------------------

const CATEGORY_ORDERED_NAMES = [
  'ParaView', // 0
  'Release Candidates', // 1
  'ParaView Server for Headless Machines', // 2
  'Plugins', // 3
  'Documentation', // 4
  'Data', // 5
  'Catalyst', // 6
  'VeloView', // 7
  'Other files', // 8
];

const CATEGORY_DESCRIPTIONS = {
  ParaView: [
    {
      os: 'Linux,Irix,Solaris,HPUX',
      description:
        'Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.',
    },
    {
      os: 'Sources',
      description: 'Source code of the ParaView suite of tools.',
    },
    {
      os: 'Windows',
      description:
        'Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, and pvbatch. Versions with MPI in the name require <a href="https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi">MS-MPI</a>.<BR/>Unzip .zip packages in a directory with a very short path if you encounter an error message about a path being too long for some files.',
    },
    {
      os: 'macOS Intel',
      description:
        '<b>For Macs with Intel x86_64 processors only.</b> Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.<BR/>For unsigned binaries such as the nightly, you will need to remove the quarantine flag by running <b style="white-space: pre;">xattr -d com.apple.quarantine /path/to/your/ParaView-X.Y.Z.app</b>.',
    },
    {
      os: 'macOS Silicon',
      description:
        '<b>For Macs with Silicon arm64 processors (M1, M2, M3) only.</b> Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.<BR/>For unsigned binaries such as the nightly, you will need to remove the quarantine flag by running <b style="white-space: pre;">\
xattr -d com.apple.quarantine /path/to/your/ParaView-X.Y.Z.app</b>.',
    },
  ],
  'Release Candidates': [
    {
      os: 'Linux,Irix,Solaris,HPUX',
      description:
        'Previews of ParaView’s next release. Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.',
    },
    {
      os: 'Sources',
      description: 'Source code preview of ParaView’s next release.',
    },
    {
      os: 'Windows',
      description:
        'Previews of ParaView’s next release. Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, and pvbatch. Versions with MPI in the name require <a href="https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi">MS-MPI</a>.<BR/>Unzip .zip packages in a directory with a very short path if you encounter an error message about a path being too long for some files.',
    },
    {
      os: 'macOS Intel',
      description:
        'Previews of ParaView’s next release <b>for Macs with Intel x86_64 processors only.</b> Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.<BR/>For unsigned binaries such as the nightly, you will need to remove the quarantine flag by running <b style="white-space: pre;">xattr -d com.apple.quarantine /path/to/your/ParaView-X.Y.Z.app</b>.',
    },
    {
      os: 'macOS Silicon',
      description:
        'Previews of ParaView’s next release <b>for Macs with Silicon arm64 processors (M1, M2, M3) only.</b> Full suite of ParaView tools, including the ParaView GUI client, pvpython, pvserver, pvbatch, and bundled MPI.<BR/>For unsigned binaries such as the nightly, you will need to remove the quarantine flag by \
running <b style="white-space: pre;">xattr -d com.apple.quarantine /path/to/your/ParaView-X.Y.Z.app</b>.',
    },
  ],
  'ParaView Server for Headless Machines': [
    {
      os: 'Sources,macOS Silicon,macOS Intel,Windows,Linux,Irix,Solaris,HPUX',
      description:
        'Suite of ParaView mostly server-side applications for rendering without an X server, including pvpython, pvserver, pvbatch, and a bundled MPI library. Does not include the ParaView GUI client.',
    },
  ],
  Plugins: [
    {
      os: 'Sources,macOS Silicon,macOS Intel,Windows,Linux,Irix,Solaris,HPUX',
      description: 'ParaView plugins',
    },
  ],
  Documentation: [
    {
      os: 'Sources,macOS Silicon,macOS Intel,Windows,Linux,Irix,Solaris,HPUX',
      description: 'Quick start, tutorial, and user guides for ParaView and Catalyst.',
    },
  ],
  Data: [
    {
      os: 'Sources,macOS Silicon,macOS Intel,Windows,Linux,Irix,Solaris,HPUX',
      description: 'Example data to load into ParaView and data used for testing.',
    },
  ],
  Catalyst: [
    {
      os: 'Sources,macOS Silicon,macOS Intel,Windows,Linux,Irix,Solaris,HPUX',
      description: 'Source archives for Catalyst Editions.',
    },
  ],
  VeloView: [],
  'Other files': [],
};

// The order is important in term of priority
const TAG_TO_CATEGORY_MAPPING = [
  { tag: 'DATA', label: CATEGORY_ORDERED_NAMES[5] },
  { tag: 'DOC', label: CATEGORY_ORDERED_NAMES[4] },
  { tag: 'PLUGIN', label: CATEGORY_ORDERED_NAMES[3] },
  { tag: 'CATALYST', label: CATEGORY_ORDERED_NAMES[6] },
  { tag: 'OFFSCREEN', label: CATEGORY_ORDERED_NAMES[2] },
  { tag: 'RC', label: CATEGORY_ORDERED_NAMES[1] },
  { tag: 'PARAVIEW', label: CATEGORY_ORDERED_NAMES[0] },
  { tag: 'VELOVIEW', label: CATEGORY_ORDERED_NAMES[7] },
];

function extractTypeFromTags(name, tags, os) {
  // binary, source, data, catalyst, plugin, demo, app
  if (tags.indexOf('DATA') !== -1) {
    return 'data';
  }
  if (tags.indexOf('DOC') !== -1) {
    return 'data';
  }
  if (tags.indexOf('PLUGIN') !== -1) {
    return 'plugin';
  }
  if (tags.indexOf('PARAVIEW') !== -1) {
    if (os.length) {
      return 'binary';
    }
    return 'source';
  }
  if (tags.indexOf('CATALYST') !== -1) {
    return 'catalyst';
  }
  if (tags.indexOf('VELOVIEW') !== -1) {
    return 'app';
  }

  // Fallback to data
  return 'data';
}

// ----------------------------------------------------------------------------
// FileName processing
// ----------------------------------------------------------------------------

// Generic feature handling
function extractFeatures(fileName, features) {
  const result = [];
  features.forEach(feature => {
    let addTag = false;
    feature.patterns.forEach(pattern => {
      if (fileName.match(pattern)) {
        addTag = true;
      }
    });
    if (addTag) {
      result.push(feature.label);
    }
  });
  return result;
}

// Process valid filename
function htmlFileToEntry(fileEntry) {
  const { name, size, date } = fileEntry;
  const version = {
    label: fileEntry.version || CURRENT_VERSION,
    number: VERSION_TO_NUMBER[fileEntry.version || CURRENT_VERSION],
  };
  const tags = extractFeatures(name, TAGS);
  const os = extractFeatures(name, OS);
  const arch = extractFeatures(name, ARCH)[0] || 'no-arch';
  const type = extractTypeFromTags(name, tags, os);
  const entry = { name, version, tags, size, date, type, arch };

  // OS handling
  entry.os = os[0] || NO_OS;

  // url extract
  entry.url = `https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=${version.label}&type=${type}&os=${entry.os}&downloadFile=${name}`;

  return entry;
}

// ----------------------------------------------------------------------------
// Entry processing
// ----------------------------------------------------------------------------

function getCategory(entry) {
  const size = TAG_TO_CATEGORY_MAPPING.length;
  for (let i = 0; i < size; i++) {
    if (entry.tags.indexOf(TAG_TO_CATEGORY_MAPPING[i].tag) !== -1) {
      return TAG_TO_CATEGORY_MAPPING[i].label;
    }
  }

  return CATEGORY_ORDERED_NAMES[size];
}

function fillData(entry) {
  if (!FILE_DATA[entry.version.label]) {
    FILE_DATA[entry.version.label] = {};
  }
  const categoryName = getCategory(entry);
  if (!FILE_DATA[entry.version.label][categoryName]) {
    FILE_DATA[entry.version.label][categoryName] = { entries: [], os: [] };
  }
  FILE_DATA[entry.version.label][categoryName].entries.push(entry);
  if (entry.os && FILE_DATA[entry.version.label][categoryName].os.indexOf(entry.os) === -1) {
    FILE_DATA[entry.version.label][categoryName].os.push(entry.os);
  }
}

function ensureParaViewCoreOS(version) {
  const catToUpdate = FILE_DATA[version].ParaView;
  if (catToUpdate) {
    ['Windows', 'Linux', 'macOS Intel'].forEach(os => {
      if (catToUpdate.os.indexOf(os) === -1) {
        catToUpdate.os.push(os);
      }
    });
  }
}

// ----------------------------------------------------------------------------
// Update content
// ----------------------------------------------------------------------------

function loadVersion(versionName) {
  CURRENT_VERSION = versionName;
  if (FILE_DATA[CURRENT_VERSION]) {
    return new Promise((resolve, reject) => {
      resolve(FILE_DATA[CURRENT_VERSION]);
    });
  }
  return new Promise((resolve, reject) => {
    fetchText(
      IS_PARAVIEW_ORG ? `/files/${versionName}/` : `./${versionName}.html`
    ).then(htmlContent => {
      parseListing(htmlContent, HTML_APACHE)
        .filter(f => f.file)
        .map(htmlFileToEntry)
        .forEach(fillData);

      resolve(FILE_DATA[CURRENT_VERSION]);
    });
  });
}

// ----------------------------------------------------------------------------

function updateWebContent(filterOS, tags) {
  const version = document.querySelector(WEB_VERSION_SELECTOR).value;
  const container = document.querySelector(WEB_CONTENT);

  if (version !== URL_PARAMS.get('version')) {
    updateURLParams({ version });
  }

  loadVersion(version).then(rawData => {
    const data = [];

    CATEGORY_ORDERED_NAMES.forEach(catergoryName => {
      if (rawData[catergoryName]) {
        const os = rawData[catergoryName].os.sort(filterBySort);
        const items = rawData[catergoryName].entries;
        const section = {
          name: catergoryName.trim(),
          label: catergoryName,
          descriptions: CATEGORY_DESCRIPTIONS[catergoryName],
          items,
        };

        // Always sort
        items.sort(entrySort);

        // Filter by OS
        if (os && os.length > 1) {
          section.filterBy = os;
        } else if (os && os.length === 1 && FILTER_BY_ORDER.indexOf(os[0])) {
          section.filterBy = os;
        } else {
          // Do not filter items if no button for selecting them
          items.forEach(item => {
            item.os = NO_FILTER;
          });
        }

        data.push(section);
      }
    });

    // Ensure ParaView has the core OSs
    ensureParaViewCoreOS(version);

    container.innerHTML = contentTemplate({ data, version, style, icons });

    // Attach filter listener
    const filterButtons = ROOT_CONTAINER.querySelectorAll(WEB_FILTERS);
    for (let i = 0; i < filterButtons.length; i++) {
      filterButtons[i].addEventListener('click', filterBy);
    }

    // Update visibility
    updateLinkVisibility(filterOS || CURRENT_OS, tags);

    // Apply additional style based on tags
    applyTagsStyles();
  });
}

// ----------------------------------------------------------------------------
// Main
// ----------------------------------------------------------------------------

const rootContainer = document.createElement('div');
rootContainer.innerHTML = rootTemplate({ style, icons });
rootContainer.classList.add('et_pb_row'); // WordPress class to make it column like
ROOT_CONTAINER.appendChild(rootContainer);

// ----------------------------------------------------------------------------
// Query filtering
// ----------------------------------------------------------------------------

function processUrlParameters() {
  if (URL_PARAMS.get('version')) {
    document.querySelector(WEB_VERSION_SELECTOR).value = URL_PARAMS.get('version');
  }
  updateWebContent(URL_PARAMS.get('filter'), URL_PARAMS.get('tags'));
}

// ----------------------------------------------------------------------------
// File listing
// ----------------------------------------------------------------------------

if (USE_LISTING_FILE) {
  fetchText(IS_PARAVIEW_ORG ? '/files/listing.txt' : './listing.txt').then(
    textContent => {
      // Handle versions
      const versionLabels = TEXT_FIND.extractVersions(textContent);
      versionLabels.forEach(label => {
        VERSION_TO_NUMBER[label] = getVersionNumber(label);
      });
      Object.keys(VERSION_TO_NUMBER)
        .map(label => ({ label, number: VERSION_TO_NUMBER[label] }))
        .sort((a, b) => b.number - a.number)
        .map(a => a.label)
        .forEach(label => {
          SORTED_VERSIONS.push(label);
        });

      // Pre-fill the data
      parseListing(textContent, TEXT_FIND)
        .filter(f => f.file)
        .map(htmlFileToEntry)
        .forEach(fillData);

      // Render Version selector
      const container = document.querySelector(VERSION_DROP_DOWN);
      container.innerHTML = versionsTemplate({
        SORTED_VERSIONS,
        CURRENT_OS,
        style,
        icons,
      });
      const select = document.querySelector(WEB_VERSION_SELECTOR);
      select.addEventListener('change', () => updateWebContent());
      select.value = SORTED_VERSIONS[1];

      // Update main page content
      processUrlParameters();
    },
    (...args) => {
      alert('The server does not seems to respond correctly, please try again at a later time.');
      console.error(...args);
    }
  );
}

// ----------------------------------------------------------------------------
// Apache directory listing
// ----------------------------------------------------------------------------

if (!USE_LISTING_FILE) {
  fetchText(IS_PARAVIEW_ORG ? '/files/' : './files.html').then(
    htmlContent => {
      // Handle versions
      const versionLabels = HTML_APACHE.extractVersions(htmlContent);
      versionLabels.forEach(label => {
        VERSION_TO_NUMBER[label] = getVersionNumber(label);
      });
      Object.keys(VERSION_TO_NUMBER)
        .map(label => ({ label, number: VERSION_TO_NUMBER[label] }))
        .sort((a, b) => b.number - a.number)
        .map(a => a.label)
        .forEach(label => {
          SORTED_VERSIONS.push(label);
        });

      // Render Version selector
      const container = document.querySelector(VERSION_DROP_DOWN);
      container.innerHTML = versionsTemplate({
        SORTED_VERSIONS,
        CURRENT_OS,
        style,
      });
      const select = document.querySelector(WEB_VERSION_SELECTOR);
      select.addEventListener('change', () => updateWebContent());
      select.value = SORTED_VERSIONS[1];

      // Update main page content
      processUrlParameters();
    },
    (...args) => {
      alert('The server does not seems to respond correctly, please try again at a later time.');
      console.error(...args);
    }
  );
}
