const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');

const entry = path.join(__dirname, './src/index.js');
const sourcePath = path.join(__dirname, './src');
const outputPath = path.join(__dirname, './dist');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry,
  output: {
    path: outputPath,
    filename: 'pv-download.js',
  },
  plugins: [
    new ExtractTextPlugin('pv-download.css'),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          { loader: 'babel-loader', options: { presets: ['es2015'] } },
        ],
      }, {
        test: /\.hbs$/i,
        use: [
          { loader: 'handlebars-loader' },
        ],
      }, {
        test: /\.mcss$/,
        use: ExtractTextPlugin.extract({
          use: [
            { loader: 'css-loader', options: { localIdentName: '[name]_[local]_[hash:base64:5]', modules: true } },
            { loader: 'postcss-loader', options: { plugins: () => [autoprefixer('last 3 version', 'ie >= 10')] } },
          ],
        }),
      }, {
        test: /\.svg$/,
        use: [
          { loader: 'svg-inline-loader' },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath,
    ],
    alias: {
      'pv': __dirname,
    },
  },
};
