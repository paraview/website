# ParaView Download Page

This project aims to provide a dynamic and more generic download page for ParaView.

## Pre-requisite

You need to have node install.

## Building

```
$ npm install
$ npm run build:release
```

## Deploying

```
$ scp ./dist/pv-download.* kitware@public:/files/
```

## Development

```
$ npm start

=> Open http://localhost:8080
```


## Updating the server side listing

```
find . -type f -ls | awk '{print $11,$8,$9,$10,$7}' | grep ./v[0-9] > listing.txt
```

## URL Filtering

Now you can filter the download page to highlight a specific binary that you want someone to download.

__Parameters__:
 - __version__ will affect the drop down value and the content of the page shown. (v5.6, nightly)
 - __filter__ allow to force a given OS rather than the detected one. (Linux, Windows, macOS, Sources)
 - __selection__ will create a border around any link that match the regexp provided while also adding the SELECTION tag to that link. (any regexp string)
 - __tags__ will hide any link that does not have the specified set of tags. We can prefix a given tag with __!__ to exclude that tag from the visible list. (RC, PARAVIEW, DOC, OFFSCREEN, MPI, CATALYST, DATA, PLUGIN, LATEST, VELOVIEW, SELECTION)

Examples:
 - /?version=nightly&filter=Linux&tags=LATEST,OFFSCREEN,!CATALYST&selection=Python3.7
 - /?version=nightly&filter=Linux&tags=LATEST,OFFSCREEN,!CATALYST,SELECTION&selection=Python3.7
 - /?filter=Linux&tags=!DOC,!DATA,SELECTION,!OFFSCREEN&selection=Python3
