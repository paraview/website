import download from './download.svg';
import link from './link-variant.svg';

export default {
  download,
  link,
};
